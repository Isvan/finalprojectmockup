package com.example.finalprojectmockup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GameSetUp extends Activity implements OnClickListener {

	Button startGame1;
	Button startGame2;
	Button startGame3;
	Button startGame4;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game_set_up);
	
		startGame1 = (Button)findViewById(R.id.btnStart);
		startGame2 = (Button)findViewById(R.id.btnStart2);
		startGame3 = (Button)findViewById(R.id.btnStart3);
		startGame4 = (Button)findViewById(R.id.btnStart4);
		
		startGame1.setOnClickListener(this);
		startGame2.setOnClickListener(this);
		startGame3.setOnClickListener(this);
		startGame4.setOnClickListener(this);
		
	}


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		//We dont actually care which button gets pressed 
		intent = new Intent(this, TheGame.class);
		startActivity(intent);
		
		
	}

	
}
