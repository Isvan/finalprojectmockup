package com.example.finalprojectmockup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainMenuActivity extends Activity implements OnClickListener {

	Button btnStartGame;
	Button btnSettings;
	Button btnHelp;
	Button btnHighScore;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);

		btnStartGame = (Button) findViewById(R.id.btnStart);
		btnSettings = (Button) findViewById(R.id.btnSettings);
		btnHelp = (Button) findViewById(R.id.btnHelp);
		btnHighScore = (Button) findViewById(R.id.btnHighScore);

		btnSettings.setOnClickListener(this);
		btnStartGame.setOnClickListener(this);
		btnHelp.setOnClickListener(this);
		btnHighScore.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent;
		switch (v.getId()) {

		case R.id.btnStart:

			
			intent = new Intent(this, GameSetUp.class);
			startActivity(intent);
			
			break;

		case R.id.btnSettings:
			intent = new Intent(this, Settings.class);
			startActivity(intent);
			break;

		case R.id.btnHighScore:
			intent = new Intent(this, HighScore.class);
			startActivity(intent);
			break;

		case R.id.btnHelp:
			intent = new Intent(this, HelpAboutUs.class);
			startActivity(intent);
			break;

		}

	}

}
