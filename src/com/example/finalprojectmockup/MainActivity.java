package com.example.finalprojectmockup;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


public class MainActivity extends Activity {

	Handler handle = new Handler();
	
	Runnable tick = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Intent intent;
			intent = new Intent(MainActivity.this, MainMenuActivity.class);
			startActivity(intent);
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//Show splash screen for 5 sec 
		handle.postDelayed(tick, 500);
		
		
	}

	public void nextScreen(){
		
	}
	


}
